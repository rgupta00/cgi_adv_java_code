package com.demo;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import java.util.*;
public class MainOneToOne {
	
	public static void main(String[] args) {
		
		
		SessionFactory factory=HibernateFactory.getSessionFactory();
		
		Session session=factory.openSession();
		
		session.getTransaction().begin();
		
		//N+1 problem
		/*
		 * performance problem
		 * fetch join: i will hib why not get all the data in one go
		 */
		
		//List<Parking>parkingList=session.createQuery("select p from Parking p").getResultList();
		List<Parking>parkingList=session.createQuery("from Parking p join fetch p.employee Employee").getResultList();
		for(Parking parking: parkingList) {
			System.out.print(parking.getParkingLocation()+":");
			Employee employee=parking.getEmployee();
			//System.out.println(employee.getEmpName());
		}
		
		
		//Parking parking= session.get(Parking.class, 5);
		
		//session.delete(parking);
		
//		
//		Employee employee1=new Employee("raj");
//		Employee employee2=new Employee("ekta");
//		Employee employee3=new Employee("gun");
//		Employee employee4=new Employee("keshav");
//		Employee employee5=new Employee("vikas");
//		
//		Parking parking1=new Parking("A12");
//		Parking parking2=new Parking("M2");
//		Parking parking3=new Parking("B2");
//		Parking parking4=new Parking("T11");
//		Parking parking5=new Parking("U12");
//		
//		
//		parking1.setEmployee(employee1);
//		parking2.setEmployee(employee2);
//		parking3.setEmployee(employee3);
//		parking4.setEmployee(employee4);
//		parking5.setEmployee(employee5);
//		
//		Session session=factory.openSession();
//		
//		session.getTransaction().begin();
//		
//		session.persist(parking1);
//		session.persist(parking2);
//		session.persist(parking3);
//		session.persist(parking4);
//		session.persist(parking5);
//		

//		session.persist(employee1);
//		session.persist(employee2);
//		session.persist(employee3);
//		session.persist(employee4);
//		session.persist(employee5);

	
		
		session.getTransaction().commit();
		
		session.close();
		factory.close();
		
	}

}
