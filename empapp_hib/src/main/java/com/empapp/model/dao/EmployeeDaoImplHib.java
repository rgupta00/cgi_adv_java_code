package com.empapp.model.dao;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

public class EmployeeDaoImplHib implements EmployeeDao{

	private SessionFactory sessionFactory;
	
	 public EmployeeDaoImplHib() {
		sessionFactory=HibernateFactory.getSessionFactory();
	}
	@Override
	public void addEmployee(Employee employee) {
		Session session=sessionFactory.openSession();
		Transaction tx=session.getTransaction();
		try {
			tx.begin();
			
			session.save(employee);
			
			tx.commit();
			
		}catch(HibernateException ex) {
			tx.rollback();
		}
		session.close();
	}

	@Override
	public void deleteEmployee(int id) {
		Session session=sessionFactory.openSession();
		Transaction tx=session.getTransaction();
		try {
			tx.begin();
			
			Employee employee=getEmployeeById(id);
			
			session.remove(employee);
			tx.commit();
			
		}catch(HibernateException ex) {
			tx.rollback();
		}
		session.close();
	}

	@Override
	public void updateEmployee(int id, int salary) {
		Session session=sessionFactory.openSession();
		Transaction tx=session.getTransaction();
		try {
			tx.begin();
			
			Employee employee=getEmployeeById(id);
			if(employee!=null) {
				employee.setSalary(salary);
				session.update(employee);
				System.out.println("updated...");
			}else {
				System.out.println("emp not found");
			}
			
			tx.commit();
			
		}catch(HibernateException ex) {
			tx.rollback();
		}
		session.close();
	}

	@Override
	public Employee getEmployeeById(int id) {
		Session session=sessionFactory.openSession();
		Employee employee=session.get(Employee.class, id);
		if(employee==null)
			throw new EmployeeNotFoundException("emp with id :"+ id + " is not found");
		
		session.close();
		return employee;
	}

	@Override
	public Employee getEmployeeByName(String name) {
		return null;
	}

	@Override
	public List<Employee> getAllEmployee() {
		Session session=sessionFactory.openSession();
		List<Employee> employees=session.createQuery("select e from Employee e").getResultList();
		session.close();
		return employees;
	}

}
