package com.empapp.model.dao;
import java.util.*;
public interface EmployeeDao {
	public void addEmployee(Employee employee);
	public void deleteEmployee(int id);
	public void updateEmployee(int id, int salary);
	public Employee getEmployeeById(int id);
	public Employee getEmployeeByName(String name);
	public List<Employee> getAllEmployee();

}
