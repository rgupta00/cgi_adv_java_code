package com.empapp.model.dao;
/*
import java.sql.*;
import java.util.*;

public class EmployeeDaoImpl implements EmployeeDao {

	private Connection connection;

	public EmployeeDaoImpl() {
		connection = ConnectionFactory.getConnection();
	}

	@Override
	public void addEmployee(Employee employee) {
		try {
			PreparedStatement pstmt = connection
					.prepareStatement("insert into  emp2(id, name, salary, age) values(?,?,?,?)");
			pstmt.setInt(1, employee.getId());
			pstmt.setString(2, employee.getName());
			pstmt.setInt(3, employee.getSalary());
			pstmt.setInt(4, employee.getAge());
			pstmt.executeUpdate();/// change the state of db

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void deleteEmployee(int id) {
		try {
			PreparedStatement pstmt = connection.prepareStatement("delete from emp2 where id=?");
			pstmt.setInt(1, id);
			pstmt.executeUpdate();/// change the state of db

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void updateEmployee(int id, int salary) {
		try {
			PreparedStatement pstmt = connection.prepareStatement("update emp2 set salary=? where id=?");
			pstmt.setInt(1, salary);
			pstmt.setInt(2, id);
		
			pstmt.executeUpdate();/// change the state of db

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public Employee getEmployeeById(int id) {
		Employee employee = null;
		try {
			PreparedStatement pstmt = connection.prepareStatement("select * from emp2 where id=?");
			pstmt.setInt(1, id);

			ResultSet rs = pstmt.executeQuery();
			if (rs.next()) {
				employee = new Employee(rs.getInt("id"), 
						rs.getString("name"), 
						rs.getInt("salary"), 
						rs.getInt("age"));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		if(employee==null) 
			throw new EmployeeNotFoundException("employee with id="+id +" is not found");
		else
			return employee;
		
			
	}

	@Override
	public Employee getEmployeeByName(String name) {
		Employee employee = null;
		try {
			PreparedStatement pstmt = connection.prepareStatement("select * from emp2 where name=?");
			pstmt.setString(1, name.trim());

			ResultSet rs = pstmt.executeQuery();
			if (rs.next()) {
				employee = new Employee(rs.getInt("id"), 
						rs.getString("name"), 
						rs.getInt("salary"), 
						rs.getInt("age"));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		if(employee==null) 
			throw new EmployeeNotFoundException("employee with name="+ name +" is not found");
		else
			return employee;
	}

	@Override
	public List<Employee> getAllEmployee() {
		List<Employee> employees=new ArrayList<Employee>();
		Employee employee;
		
		try {
			PreparedStatement pstmt = connection.prepareStatement("select * from emp2 ");
			
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				employee = new Employee(rs.getInt("id"), 
						rs.getString("name"), 
						rs.getInt("salary"), 
						rs.getInt("age"));
				
				employees.add(employee);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return employees;
		
	}

}
*/