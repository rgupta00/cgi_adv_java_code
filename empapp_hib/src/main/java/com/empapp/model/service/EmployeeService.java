package com.empapp.model.service;

import java.util.List;

import com.empapp.model.dao.Employee;

public interface EmployeeService {
	public void addEmployee(Employee employee);
	public void deleteEmployee(int id);
	public void updateEmployee(int id, int salary);
	public Employee getEmployeeById(int id);
	public Employee getEmployeeByName(String name);
	public List<Employee> getAllEmployee();
}
