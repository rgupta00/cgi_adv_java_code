package com.empapp.model.service;

import java.util.List;

import com.empapp.model.dao.Employee;
import com.empapp.model.dao.EmployeeDao;
import com.empapp.model.dao.EmployeeDaoImplHib;
//it can do some extra work: sec, tx logging: spring*
public class EmployeeServiceImpl implements EmployeeService {

	private EmployeeDao employeeDao;
	
	public EmployeeServiceImpl(){
		employeeDao=new EmployeeDaoImplHib();
	}
	
	@Override
	public void addEmployee(Employee employee) {
		employeeDao.addEmployee(employee);
	}

	@Override
	public void deleteEmployee(int id) {
		employeeDao.deleteEmployee(id);
	}

	@Override
	public void updateEmployee(int id, int salary) {
		employeeDao.updateEmployee(id, salary);
	}

	@Override
	public Employee getEmployeeById(int id) {
		return employeeDao.getEmployeeById(id);
	}

	@Override
	public Employee getEmployeeByName(String name) {
		return employeeDao.getEmployeeByName(name);
	}

	@Override
	public List<Employee> getAllEmployee() {
		return employeeDao.getAllEmployee();
	}

}
