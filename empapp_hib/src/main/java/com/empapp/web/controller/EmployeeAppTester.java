package com.empapp.web.controller;

import java.util.*;

import com.empapp.model.dao.Employee;
import com.empapp.model.dao.EmployeeNotFoundException;
import com.empapp.model.service.EmployeeService;
import com.empapp.model.service.EmployeeServiceImpl;

public class EmployeeAppTester {

	public static void main(String[] args) {
		EmployeeService employeeService = new EmployeeServiceImpl();

		List<Employee> employees = employeeService.getAllEmployee();

		System.out.println("------Printing all emp------");
		// printEmployees(employees);

		//System.out.println("----adding an emp------------");

//		Employee employee1 = new Employee("teja", 67, 25);
//		Employee employee2 = new Employee("ravi", 57, 29);
//		Employee employee3 = new Employee("amit", 60, 35);
//		
//
//		employeeService.addEmployee(employee1);
//		employeeService.addEmployee(employee2);
//		employeeService.addEmployee(employee3);
		

		//System.out.println("----updating an emp------------");
		//System.out.println(employeeService.getEmployeeById(2));
		
		//employeeService.updateEmployee(2, 72);

		//System.out.println(employeeService.getEmployeeById(2));
		
		//System.out.println("----delete an emp------------");

		//employeeService.deleteEmployee(2);
		
//		System.out.println("find and print an employee");
//		try{
//			Employee employee=employeeService.getEmployeeByName("umesh");
//			System.out.println(employee);
//		}catch(EmployeeNotFoundException ex) {
//			System.out.println(ex.getMessage());
//		}
		
		employees = employeeService.getAllEmployee();
		printEmployees(employees);

	}

	private static void printEmployees(List<Employee> employees) {
		for (Employee employee : employees) {
			System.out.println(employee);
		}
	}
}
