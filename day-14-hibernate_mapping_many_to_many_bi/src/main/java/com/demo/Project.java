package com.demo;
import java.util.*;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
@Table(name="project_m2m")
@Entity
public class Project {
	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	private int projectId;
	private String projectName;

	@JoinTable(name="emp_proj_join", joinColumns = @JoinColumn(name="pid_fk"),
			inverseJoinColumns = @JoinColumn(name="eid_fk"))
	@ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
	private List<Employee> employees = new ArrayList<Employee>();

	public int getProjectId() {
		return projectId;
	}

	public void setProjectId(int projectId) {
		this.projectId = projectId;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public List<Employee> getEmployees() {
		return employees;
	}

	public void setEmployees(List<Employee> employees) {
		this.employees = employees;
	}

	public Project(String projectName) {
		this.projectName = projectName;
	}

	public Project() {}

	@Override
	public String toString() {
		return "Project [projectId=" + projectId + ", projectName=" + projectName + ", employees=" + employees + "]";
	}

	
}
