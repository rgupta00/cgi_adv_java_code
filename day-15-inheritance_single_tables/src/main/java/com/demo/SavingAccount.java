package com.demo;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "account_single_table")
@DiscriminatorValue("C")
public class SavingAccount extends Account {
	
	private double intrestRate;

	public double getIntrestRate() {
		return intrestRate;
	}

	public void setIntrestRate(double intrestRate) {
		this.intrestRate = intrestRate;
	}

	public SavingAccount(String accountHolderName, double balance, double intrestRate) {
		super(accountHolderName, balance);
		this.intrestRate = intrestRate;
	}

	@Override
	public String toString() {
		return super.toString()+  "SavingAccount [intrestRate=" + intrestRate + "]";
	}

	public SavingAccount() {}

	
}