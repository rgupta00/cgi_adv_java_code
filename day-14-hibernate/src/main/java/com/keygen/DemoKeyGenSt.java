package com.keygen;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.demo.factory.HibernateFactory;

public class DemoKeyGenSt {
	
	public static void main(String[] args) {
		SessionFactory factory=HibernateFactory.getSessionFactory();
		Session session=factory.openSession();
		
		session.getTransaction().begin();
		Customer customer=new Customer("suman", "55545545");
		session.save(customer);
		Address address=new Address("A12", "delhi","543545454");
		session.save(address);
		session.getTransaction().commit();
		
		session.close();
		
		factory.close();
		
	}

}
