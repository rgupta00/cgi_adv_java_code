package com.more_ann;

import java.util.Date;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.demo.factory.HibernateFactory;

public class MoreAnn {
	
	public static void main(String[] args) {
		SessionFactory factory=HibernateFactory.getSessionFactory();
		Session session=factory.openSession();
		
		
		session.getTransaction().begin();
		Customer customer=new Customer("raj", "raj@gmail.com",
				"434343443", "mytoken", new Date(), new Date(), Gender.M);
		
		Customer customer2=new Customer("ekta", "ekta@gmail.com",
				"434343443", "mytoken", new Date(), new Date(), Gender.F);
		
		session.save(customer2);
		session.save(customer);
		session.getTransaction().commit();// write behind approach
		
		
		
		session.close();
		factory.close();
		
	}

}
