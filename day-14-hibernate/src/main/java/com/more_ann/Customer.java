package com.more_ann;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Table(name = "customer_table_more_anno")
@Entity
public class Customer {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	
	@Column(name = "customer_name" , length = 100, nullable = false)
	private String name;
	
	@Column(name = "customer_email" , length = 100, nullable = false, unique = true)
	private String email;
	
	@Column(name = "customer_phone" , length = 100, nullable = false)
	private String phone;
	
	@Transient // not stored in db
	private String tempToken;
	
	@Column(name = "customer_date",nullable = false)
	@Temporal(TemporalType.DATE)
	private Date dob;
	
	@Column(name = "customer_logintime" ,nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date loginTime;
	
	@Enumerated(EnumType.STRING)
	private Gender gender;

	public Customer(String name, String email, String phone, String tempToken, Date dob, Date loginTime,
			Gender gender) {
		
		this.name = name;
		this.email = email;
		this.phone = phone;
		this.tempToken = tempToken;
		this.dob = dob;
		this.loginTime = loginTime;
		this.gender = gender;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getTempToken() {
		return tempToken;
	}

	public void setTempToken(String tempToken) {
		this.tempToken = tempToken;
	}

	public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	public Date getLoginTime() {
		return loginTime;
	}

	public void setLoginTime(Date loginTime) {
		this.loginTime = loginTime;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	@Override
	public String toString() {
		return "Customer [id=" + id + ", name=" + name + ", email=" + email + ", phone=" + phone + ", tempToken="
				+ tempToken + ", dob=" + dob + ", loginTime=" + loginTime + ", gender=" + gender + "]";
	}

	public Customer() {}
	
	
}
