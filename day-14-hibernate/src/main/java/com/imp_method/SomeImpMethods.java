package com.imp_method;

import java.util.*;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.demo.factory.HibernateFactory;

public class SomeImpMethods {

	public static void main(String[] args) {
		SessionFactory factory = HibernateFactory.getSessionFactory();

		Session session = factory.openSession();

		Transaction tx = session.getTransaction();

		try {
			// save() and persist()
			tx.begin();
			
			Customer customer=session.get(Customer.class,1);
			System.out.println(customer);
			
			
			try {
				Thread.currentThread().sleep(20000);
			}catch(InterruptedException ex) {
				ex.printStackTrace();
			}
			session.refresh(customer);
			System.out.println(customer);
			
			
//			Customer customer5 = new Customer("umesh", "353545545");
//			session.persist(customer5);
//			Customer customer1=new Customer("ravi", "353545545");
//			Customer customer2=new Customer("amit", "353505545");
//			Customer customer3=new Customer("kapil", "753545545");
//			
//			Customer customer4=new Customer("ts", "350045545");
//			
//			session.save(customer1);
//			session.save(customer2);
//			session.save(customer3);
//			session.save(customer4);
			tx.commit();
		} catch (HibernateException e) {
			tx.rollback();
			e.printStackTrace();
		}

//		



		session.close();

		factory.close();

	}
}
