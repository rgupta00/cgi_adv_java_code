package com.lifecycle;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.demo.factory.HibernateFactory;

public class HibernateLifeCycle {
	
	public static void main(String[] args) {
		SessionFactory factory=HibernateFactory.getSessionFactory();
		Session session=factory.openSession();
		
		//transient ------- persited -------detached
		
		//persisted
		
		
		session.getTransaction().begin();
		Customer customer=session.get(Customer.class, 921);
		customer.setPhone("9357754545");
		
		
		
		//session.update(customer);
		
		//dirty checking
		session.getTransaction().commit();// write behind approach
		
		
		//Customer customer=session.load(Customer.class, 19);
		
		
		
		//get vs load
		//Customer customer=session.load(Customer.class, 19);
		//LazyInitializationException
		
		//u can remove that object from the cached
		//session.evict(customer);
		//Customer customer2=session.get(Customer.class, 19);
		
		session.close();
		//detached object
		System.out.println(customer);
		factory.close();
		
	}

}
