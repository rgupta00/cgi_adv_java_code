package com.demo;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.demo.factory.HibernateFactory;

public class DeleteCustomer {

	public static void main(String[] args) {
		SessionFactory factory=HibernateFactory.getSessionFactory();
		//from sessionfactory -> session
		Session session=factory.openSession();
		//start the tx
		Transaction tx= session.getTransaction();
		try {
			tx.begin();
			
			//first find the customer 
			Customer customer=session.get(Customer.class, 121);
			if(customer!=null) {
				session.delete(customer);
			}
			tx.commit();
			
		}catch(HibernateException ex) {
			ex.printStackTrace();
			tx.rollback();
		}
	
		
		//close the session
		session.close();
		//close the sf
		
		factory.close();
		
	}
}
