package com.demo;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

import com.demo.factory.HibernateFactory;

public class AddCustomer {
	
	public static void main(String[] args) {
		
		
		SessionFactory factory=HibernateFactory.getSessionFactory();
		
		
		Session session=factory.openSession();
		
		session.getTransaction().begin();
		
		Customer customer2=new Customer(110, "ekta", "55904545");
		Customer customer3=new Customer(19, "keshav", "55900545");
		Customer customer4=new Customer(191, "amit", "5804545");
		Customer customer5=new Customer(131, "sumit", "66904545");
		
		session.save(customer2);
		session.save(customer3);
		session.save(customer4);
		session.save(customer5);
		
		
		System.out.println("customer is saved...");
		
		session.getTransaction().commit();
		
		session.close();
		factory.close();
		
	}

}
