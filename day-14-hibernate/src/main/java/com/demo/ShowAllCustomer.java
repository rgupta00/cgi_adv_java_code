package com.demo;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import java.util.*;
import com.demo.factory.HibernateFactory;

public class ShowAllCustomer {

	public static void main(String[] args) {
		//show all customers
		SessionFactory factory=HibernateFactory.getSessionFactory();
		
		Session session=factory.openSession();
		System.out.println(session);
		
		//this time i dnot need tx: i am not changing the state of db, i dont need tx
		//SQL X		HQL: hibernate Query langage
		
		//hibernate insulate you from the choice of db: HQL oo flavour of SQL
		
		List<Customer> customers=session
				.createQuery("select c from Customer c").getResultList();
		
		customers.forEach(c-> System.out.println(c));
		System.out.println(session);
		session.close();
		
		factory.close();
		
	}
}













