package com.composite_pk;

import java.io.Serializable;

import javax.persistence.Embeddable;

//i want to create a compoite pk
/*
 * 1. must have default ctr, it must imp ... Serializable
 * 2. apply ann @Embeddable on the class that hold compsite pk
 */
@Embeddable
public class CustomerKey implements Serializable{
	private int id;
	private String regNo;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getRegNo() {
		return regNo;
	}
	public void setRegNo(String regNo) {
		this.regNo = regNo;
	}
	public CustomerKey(int id, String regNo) {
		this.id = id;
		this.regNo = regNo;
	}
	public CustomerKey() {}
	@Override
	public String toString() {
		return "CustomerKey [id=" + id + ", regNo=" + regNo + "]";
	}
	
	
}
