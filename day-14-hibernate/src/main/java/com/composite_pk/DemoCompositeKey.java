package com.composite_pk;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.demo.factory.HibernateFactory;

public class DemoCompositeKey {
	
	public static void main(String[] args) {
		SessionFactory factory=HibernateFactory.getSessionFactory();
		Session session=factory.openSession();
		
		session.getTransaction().begin();
		CustomerKey customerKey=new CustomerKey(121, "AWQ123");
		
		Customer customer=new Customer(customerKey, "suman", "55545545");
		
		
		session.save(customer);
		
		session.getTransaction().commit();
		
		session.close();
		
		factory.close();
		
	}

}
