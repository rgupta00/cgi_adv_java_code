package com.composite_pk;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "customer_table_composite_key")
@Entity
public class Customer {

	@Id
	private CustomerKey customerKey;
	
	private String name;
	private String phone;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public Customer() {}
	

	
	public Customer(CustomerKey customerKey, String name, String phone) {
		this.customerKey = customerKey;
		this.name = name;
		this.phone = phone;
	}
	@Override
	public String toString() {
		return "Customer [customerKey=" + customerKey + ", name=" + name + ", phone=" + phone + "]";
	}
	
}
