package com.demo;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import java.util.*;

public class MainOneToManyUni {

	public static void main(String[] args) {

		SessionFactory factory = HibernateFactory.getSessionFactory();

		Session session = factory.openSession();

		session.getTransaction().begin();

		Department department=session.get(Department.class, 1);
		session.delete(department);
		// we want to get dept and emp infor
		//from Parking p join fetch p.employee Employee
//		List<Department> departments = session.createQuery
//				("from Department").getResultList();
//
//		
//		
//		for (Department dept : departments) {
//			System.out.println(dept.getDetpName());
//			System.out.println("------------------------------");
//			System.out.println(dept.getEmployees());
//		}

		session.getTransaction().commit();

		session.close();
		factory.close();
//
//		Department department1=new Department("IT");
//		Department department2=new Department("sales");
//		Department department3=new Department("mkt");
//		Department department4=new Department("r&d");
//		
//		Employee employee1=new Employee("raja");
//		Employee employee2=new Employee("amit");
//		Employee employee3=new Employee("sumit");
//		Employee employee4=new Employee("ekta");
//
//		Employee employee5=new Employee("keshav");
//		Employee employee6=new Employee("gunika");
//		Employee employee7=new Employee("rajiv");
//		
//		department1.getEmployees().add(employee1);
//		department1.getEmployees().add(employee2);
//		
//		department2.getEmployees().add(employee3);
//		department2.getEmployees().add(employee4);
//		
//		department3.getEmployees().add(employee5);
//		department3.getEmployees().add(employee6);
//		department3.getEmployees().add(employee7);
//		
//		employee1.setDepartment(department1);
//		employee2.setDepartment(department1);
//		
//		
//		employee3.setDepartment(department2);
//		employee4.setDepartment(department2);
//		
//		employee5.setDepartment(department3);
//		employee6.setDepartment(department3);
//		employee7.setDepartment(department3);
//		
//		
//		
//		session.save(department1);
//		session.save(department2);
//		session.save(department3);
//		session.save(department4);
//		
//		
//		session.save(employee1);
//		session.save(employee2);
//		session.save(employee3);	
//		session.save(employee4);
//		session.save(employee5);
//		session.save(employee6);
//		session.save(employee7);
//	

	}

}
