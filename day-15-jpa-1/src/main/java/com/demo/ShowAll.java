package com.demo;
import java.util.*;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

public class ShowAll {

	public static void main(String[] args) {
		
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("cgi_app");
		EntityManager em = emf.createEntityManager();

		List<Customer> customers = em.createQuery("select c from Customer c").getResultList();
		customers.forEach(c-> System.out.println(c));
		em.close();
		emf.close();

	}

}
