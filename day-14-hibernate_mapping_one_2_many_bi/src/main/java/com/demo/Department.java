package com.demo;
import java.util.*;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.BatchSize;

@Table(name="dept_onetomany_bi")
@Entity
public class Department {
	
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int deptId;
	
	@Column(name = "dept_name", length = 100, nullable = false)
	private String detpName;
	
	@OneToMany(mappedBy = "department")
	private List<Employee>employees=new ArrayList<Employee>();
	
	
	public int getDeptId() {
		return deptId;
	}
	public void setDeptId(int deptId) {
		this.deptId = deptId;
	}
	public String getDetpName() {
		return detpName;
	}
	public void setDetpName(String detpName) {
		this.detpName = detpName;
	}
	public List<Employee> getEmployees() {
		return employees;
	}
	public void setEmployees(List<Employee> employees) {
		this.employees = employees;
	}
	public Department(String detpName) {
		this.detpName = detpName;
	}
	public Department() {}
	
	@Override
	public String toString() {
		return "Dept [deptId=" + deptId + ", detpName=" + detpName + ", employees=" + employees + "]";
	}
	
	
	
}
