package com.demo;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import java.util.*;

public class DemoInhertiance {

	public static void main(String[] args) {

		SessionFactory factory = HibernateFactory.getSessionFactory();

		Session session = factory.openSession();

		session.getTransaction().begin();
		
		//left outer join is required :( best way as per normalization theory....
		List<Account> accounts=session.createQuery("select a from Account a")
				.getResultList();
		
		accounts.forEach(a-> System.out.println(a));
//		
//		Account account=new SavingAccount("ekta", 3000, 5);
//		Account account2=new CurrentAccount("raj", 9000, 6000);
//		
//		session.save(account2);
//		session.save(account);
//		
		session.getTransaction().commit();

		session.close();
		factory.close();

	}

}
