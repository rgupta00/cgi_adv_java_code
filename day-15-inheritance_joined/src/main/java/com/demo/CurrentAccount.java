package com.demo;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
public class CurrentAccount extends Account {
	
	private double overdraft;

	public double getOverdraft() {
		return overdraft;
	}

	public void setOverdraft(double overdraft) {
		this.overdraft = overdraft;
	}

	public CurrentAccount(String accountHolderName, double balance, double overdraft) {
		super(accountHolderName, balance);
		this.overdraft = overdraft;
	}

	@Override
	public String toString() {
		return super.toString()+ "CurrentAccount [overdraft=" + overdraft + "]";
	}

	public CurrentAccount() {}

	

}
