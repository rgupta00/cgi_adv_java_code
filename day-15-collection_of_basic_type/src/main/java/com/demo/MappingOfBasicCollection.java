package com.demo;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import java.util.*;

public class MappingOfBasicCollection {

	public static void main(String[] args) {

		SessionFactory factory = HibernateFactory.getSessionFactory();

		Session session = factory.openSession();

		session.getTransaction().begin();
		
		Employee employee=new Employee("rajesh");
		employee.getPhones().add("554545545");
		employee.getPhones().add("599545545");
		employee.getPhones().add("504545545");
		
		
		Employee employee2=new Employee("ekta");
		employee2.getPhones().add("554500545");
		employee2.getPhones().add("889545545");
		employee2.getPhones().add("994545545");
		
		session.persist(employee);
		session.persist(employee2);
		
		session.getTransaction().commit();

		session.close();
		factory.close();

	}

}
